import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';

import { SigninComponent } from './signin/signin.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path:'login',
    //component:LoginComponent ,
    component:SigninComponent
  },
  {
    path:'home',
    //component:LoginComponent ,
    component:HomeComponent
  }
  ,
  {
    path:'register',
    //component:LoginComponent ,
    component:RegisterComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthenticationRoutingModule { }

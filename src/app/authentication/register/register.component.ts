import { Component, OnInit } from '@angular/core';
import { FormationService } from 'src/app/gestion-formation/services/formation.service';
import { ParticipantService } from 'src/app/gestion-participant/services/participant.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.sass']
})
export class RegisterComponent implements OnInit {


  formations = [];

  formation:any;
  tel:any;
  prenom:any;
  nom:any;

  constructor(private formationService : FormationService, private participantService : ParticipantService) { }

  ngOnInit() {
    this.formationService.getFormationListService().subscribe(
      data =>
      {
        console.log(data);
        this.formations=data;
      }
    )
  }

  subscribe()
  {
    if (this.prenom && this.nom && this.tel && this.formation)
    {
      let p ={
        nom: this.nom, 
        prenom: this.prenom, 
        telephone:this.tel,
        formation:JSON.stringify(this.formation)
      } ;

      console.log(p);
      this.participantService.addParticipantNonConfirmeService(p).subscribe(
        data => {
          alert("inscription effectuée avec succès !")
        }
      )

      this.participantService.getParticipantNonConfirmeListService().subscribe(data=>
        {
          console.log(data);
        })
    }
    else{
      alert("Vérifiez vos données !")
    }
  }

}

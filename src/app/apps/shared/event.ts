import { StringifyOptions } from 'querystring';

export class Event {

    id : number ;
    title : string ;
    start : String ;
    end ;

}

import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  constructor(private http : HttpClient) { }

  url :string ;
  //****************************************************************************** */
  getEventList():Observable<any>{
    return this.http.get(this.url) ;

  }
  //****************************************************************************** */
  getOneEvent():Observable<any>{
    return this.http.get(this.url) ;

  }
  //****************************************************************************** */
  addNewEvent(event:Object):Observable<any>{
   return this.http.post(this.url,event) ;
  }
  //****************************************************************************** */
  deleteEvent(id:number):Observable<any>{
    return this.http.delete(this.url+id)

  }




}

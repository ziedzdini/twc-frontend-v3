import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppsRoutingModule } from './apps-routing.module';
import { CalendarComponent } from './calendar/calendar.component';
import { FullCalendarModule } from '@fullcalendar/angular';
import { DragulaModule } from 'ng2-dragula';
import { EventsComponent } from './events/events.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [CalendarComponent, EventsComponent],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    AppsRoutingModule,
    FullCalendarModule,
    DragulaModule.forRoot()
  ]
  
})
export class AppsModule { }

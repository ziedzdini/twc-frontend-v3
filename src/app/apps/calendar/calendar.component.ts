import { Component, ViewChild, TemplateRef } from '@angular/core';
import { FullCalendarComponent } from '@fullcalendar/angular';
import { EventInput } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { FormationService } from 'src/app/gestion-formation/services/formation.service';
import { SessionService } from 'src/app/gestion-session/service/session.service';



var d = new Date,
    date = d.getDate(),
    month = d.getMonth(),
    year = d.getFullYear();

@Component({
    selector: 'app-calendar',
    templateUrl: './calendar.component.html',
    styleUrls: ['./calendar.component.sass']
})
export class CalendarComponent {
    @ViewChild('roleTemplate', { static: true }) roleTemplate: TemplateRef<any>;
    @ViewChild('calendar', { static: false }) calendarComponent: FullCalendarComponent; // the #calendar in the template
    addEventForm: FormGroup;





    constructor(private fb: FormBuilder, private sessionService: SessionService) { }
    ngOnInit() {
        this.addEventForm = this.fb.group(
            {
                id: new FormControl(),
                title: ['', Validators.required],
                start: ['', Validators.required],
                end: ['', Validators.required],
            }
        )

        this.sessionService.getSessionListService().subscribe(
            result => {
                console.log(result);
                //
                let t = []
                result.forEach(s => {
                   
                    t.push(
                        {
                            id: s.id,
                            title: s.nom,
                            start: (new Date(s.datedebut)).setHours(9),
                            end: new Date(s.datefin),
                        url: "http://localhost:4200/#/gestion-session/participant-confirme/" + s.id,
                            backgroundColor: '#9b59b6'
                        }
                    );

                });
                this.calendarEvents = t;

            }
        );
    }


    calendarVisible = true;
    calendarPlugins = [dayGridPlugin];
    calendarWeekends = true;
    calendarEvents: EventInput[] = [];

    /* [
        {
            title: "Conference",
            start: new Date(year, month, date - 4, 0, 0),
            end: new Date(year, month, date - 2, 0, 0),
        }, {
            title: "Holiday",
            start: new Date(year, month, date - 10, 9, 0),
            end: new Date(year, month, date - 8, 0, 0),
        }, {
            title: "Repeating Event",
            start: new Date(year, month, date + 5, 16, 0),
            allDay: !1,
        }, {
            title: "Meeting",
            start: new Date(year, month, date, 10, 30),
            allDay: !1
        }, {
            title: "Result Day",
            start: new Date(year, month, date + 7, 19, 0),
            end: new Date(year, month, date + 1, 22, 30),
            allDay: !1
        }, {
            id: 1,
            title: "Click for Google",
            start: new Date(year, month, 27),
            end: new Date(year, month, 30),

            url: "http://google.com/"
        }, 
        { title: 'event 2', start: '12/08/2020', end: '12/08/2020', 
        backgroundColor: '#9b59b6' }
    ];
    */


    handleDateClick(arg) {
        alert('date click! ' + arg.dateStr)
    }
    //event managment

    eventlist() {

    }

    eventadd() {

    }
    deleteEvents() {

    }
}

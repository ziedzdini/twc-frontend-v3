import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from './authentication/services/auth-guard.service';

const routes: Routes = [
    {
        path: 'dashboard',
        canActivate: [AuthGuardService],
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
    },
    {
        path: 'gestion-formateur',
        canActivate: [AuthGuardService],
        loadChildren: () => import('./gestion-formateur/gestion-formateur.module').then(m => m.GestionFormateurModule)
    },
    {
        path: 'gestion-categorie',
        canActivate: [AuthGuardService],
        loadChildren: () => import('./gestion-categorie/gestion-categorie.module').then(m => m.GestionCategorieModule)
    },
    {

        path: 'gestion-formation',
        canActivate: [AuthGuardService],
        loadChildren: () => import('./gestion-formation/gestion-formation.module').then(m => m.GestionFormationModule)
    },
    {

        path: 'gestion-session',
        canActivate: [AuthGuardService],
        loadChildren: () => import('./gestion-session/gestion-session.module').then(m => m.GestionSessionModule)
    },
    {

        path: 'gestion-comptes',
        canActivate: [AuthGuardService],
        loadChildren: () => import('./gestion-comptes/gestion-comptes.module').then(m => m.GestionComptesModule)
    },

    {

        path: 'gestion-participant',
        canActivate: [AuthGuardService],
        loadChildren: () => import('./gestion-participant/gestion-participant.module').then(m => m.GestionParticipantModule)
    },

    {

        path: 'email',
        canActivate: [AuthGuardService],
        loadChildren: () => import('./boite-reception/boite-reception.module').then(m => m.BoiteReceptionModule)
    },

    {

        path: 'authentication',
        loadChildren: () => import('./authentication/authentication.module').then(m => m.AuthenticationModule)
    },

    {
        path: 'apps',
        canActivate: [AuthGuardService],
        loadChildren: () => import('./apps/apps.module').then(m => m.AppsModule)
    },


    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }

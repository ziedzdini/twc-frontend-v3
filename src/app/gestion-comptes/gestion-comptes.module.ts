import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {GestionComptesRoutingModule } from './gestion-comptes-routing.module';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddComponent } from './add/add.component';
import { ListComponent } from './list/list.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';


@NgModule({
  declarations: [ AddComponent, ListComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    GestionComptesRoutingModule,
    NgxDatatableModule
  ],
  providers:
  []
})
export class  GestionComptesModule { }

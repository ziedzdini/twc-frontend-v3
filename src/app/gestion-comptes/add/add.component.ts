import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';

declare const $: any;

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.sass']
})
export class AddComponent implements OnInit {
  addCompteForm: FormGroup;

  isUpdate = false;

  constructor(private router: Router, private bf: FormBuilder, private auth: AuthenticationService, private activatedRoute: ActivatedRoute) {

    this.addCompteForm = this.bf.group({
      username: new FormControl(),
      email: new FormControl(),
      password: new FormControl(),
      role: new FormControl()
    });

  }

  t = "ADMIN";
  ngOnInit() {
    $('select').formSelect();

    let pathId = this.activatedRoute.snapshot.paramMap.get("id");
    if (pathId) {
      this.isUpdate = true;

      this.auth.get(parseInt(pathId)).subscribe(data => {



        let initialRole = "UTILISATEUR";
        console.log(data.roles);
        //
        data.roles.forEach(r => {
          if (r.name === "ROLE_ADMIN") {
            initialRole="ADMIN";
           }
        })

        //
        this.addCompteForm = this.bf.group({
          id:[data.id],
          username: [data.username],
          email: [data.email],
          password: [data.password],
          role: [initialRole]
        });
        //

        /*
        this.addCompteForm.controls['role'].setValue("ADMIN", {onlySelf: true});
        this.addCompteForm.controls['role'].updateValueAndValidity();
        */
      });


    }
  }



  addCompte(form: FormGroup) {

    console.log(form);


if(!this.isUpdate)
{

    let u = {
      "username": form.value.username,
      "password": form.value.password,
      "email": form.value.email,
      "role": ["user"]
    };

    if (form.value.role === 'ADMIN') {
      u.role.push("admin");
    }

    console.log(u);

    this.auth.signUp(u).subscribe(data => {
      console.log(data);
      form.reset();

      this.showNotification("bg-green", "Add Record Successfully", "bottom", "right", "animated fadeInRight", "animated fadeOutRight")
      this.router.navigate(['gestion-comptes/accounts']);
    });

  }
  else{

    let u = {
      "id": form.value.id,
      "username": form.value.username,
      //"password": form.value.password,
      "email": form.value.email,
      "role": ["user"]
    };

    if (form.value.role === 'ADMIN') {
      u.role.push("admin");
    }

    console.log(u);

    this.auth.update(u).subscribe(data => {
      console.log(data);
      form.reset();

      this.showNotification("bg-orange", "Update Record Successfully", "bottom", "right", "animated fadeInRight", "animated fadeOutRight")
      this.router.navigate(['gestion-comptes/accounts']);
    });


  }



  }
  clear() {
    this.addCompteForm.reset();
  }

  showNotification(colorName, text, placementFrom, placementAlign, animateEnter, animateExit) {
    if (colorName === null || colorName === '') { colorName = 'bg-black'; }
    if (text === null || text === '') { text = 'Turning standard Bootstrap alerts'; }
    if (animateEnter === null || animateEnter === '') { animateEnter = 'animated fadeInDown'; }
    if (animateExit === null || animateExit === '') { animateExit = 'animated fadeOutUp'; }
    var allowDismiss = true;

    $.notify({
      message: text
    },
      {
        type: colorName,
        allow_dismiss: allowDismiss,
        newest_on_top: true,
        timer: 1000,
        placement: {
          from: placementFrom,
          align: placementAlign
        },
        animate: {
          enter: animateEnter,
          exit: animateExit
        },
        template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (allowDismiss ? "p-r-35" : "") + '" role="alert">' +
          '<span data-notify="icon"></span> ' +
          '<span data-notify="title">{1}</span> ' +
          '<span data-notify="message">{2}</span>' +
          '<div class="progress" data-notify="progressbar">' +
          '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
          '</div>' +
          '<a href="{3}" target="{4}" data-notify="url"></a>' +
          '</div>'
      });
  }
}

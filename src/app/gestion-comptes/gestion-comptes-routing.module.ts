import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddComponent } from './add/add.component';
import { ListComponent } from './list/list.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'accounts',
    pathMatch: 'full'
  },
  {
    path: 'accounts',
    //component:LoginComponent ,
    component: ListComponent
  }, 
  {
    path: 'new',
    //component:LoginComponent ,
    component: AddComponent
  },
  {
    path: 'update/:id',
    //component:LoginComponent ,
    component: AddComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GestionComptesRoutingModule { }

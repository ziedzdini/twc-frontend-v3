import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
declare const $: any;

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;

  constructor(private auth: AuthenticationService, private router : Router) { }

  rows = [];
  selectedName: string = "";





  columns = [
    { name: 'username' }, { name: 'email' }, { name: 'password' }, { name: 'roles' }
  ];

  allColumns = [{ name: 'username' }, { name: 'email' }, { name: 'password' }, { name: 'roles' }];

  accounts = [];
  data = [];
  filteredData = [];


  //********************************************************************************** */

  ngOnInit() {


    this.reloadDataSearch();
    this.reloadData();
    console.log(this.accounts);
  }




  //************************************************************************************** */



  //*********************************************************************************** */



  //***************************************************************************** */

  reloadData() {
    this.auth.users().subscribe(data => {
      this.accounts = data;
      this.accounts.forEach(u => {
        u.password = "*****";
        let r="";
        u.roles.forEach(rl => {
          r+=rl.name+" | ";
        }); 
        u.roles = r;
      });
      console.log(this.accounts);
    });

  }

  //********************************************************* *
  reloadDataSearch() {
    this.auth.users().subscribe(data => this.filteredData = data);

  }
  //*********************************************************************************** */








  //************************************************************************************************* */
  filterDatatable(event) {
    // get the value of the key pressed and make it lowercase
    let val = event.target.value.toLowerCase();
    // get the amount of columns in the table
    let colsAmt = this.columns.length;
    // get the key names of each column in the dataset
    let keys = Object.keys(this.filteredData[0]);
    // assign filtered matches to the active datatable
    this.accounts = this.filteredData.filter(function (item) {
      // iterate through each row's column data
      for (let i = 0; i < colsAmt; i++) {
        // check for a match
        if (item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 || !val) {
          // found match, return true to add to result set
          return true;
        }
      }
    });
    // whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  getId(min, max) { // min and max included 
    return Math.floor(Math.random() * (max - min + 1) + min);
  }
  showNotification(colorName, text, placementFrom, placementAlign, animateEnter, animateExit) {
    if (colorName === null || colorName === '') { colorName = 'bg-black'; }
    if (text === null || text === '') { text = 'Turning standard Bootstrap alerts'; }
    if (animateEnter === null || animateEnter === '') { animateEnter = 'animated fadeInDown'; }
    if (animateExit === null || animateExit === '') { animateExit = 'animated fadeOutUp'; }
    var allowDismiss = true;

    $.notify({
      message: text
    },
      {
        type: colorName,
        allow_dismiss: allowDismiss,
        newest_on_top: true,
        timer: 1000,
        placement: {
          from: placementFrom,
          align: placementAlign
        },
        animate: {
          enter: animateEnter,
          exit: animateExit
        },
        template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (allowDismiss ? "p-r-35" : "") + '" role="alert">' +
          '<span data-notify="icon"></span> ' +
          '<span data-notify="title">{1}</span> ' +
          '<span data-notify="message">{2}</span>' +
          '<div class="progress" data-notify="progressbar">' +
          '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
          '</div>' +
          '<a href="{3}" target="{4}" data-notify="url"></a>' +
          '</div>'
      });
  }

  ///*********************************************************************** */



  deleteRow(row)
  {
    console.log(row);
    if(window.confirm("êtes-vous sûr de vouloir supprimer ?"))
    {
      this.auth.delete(row.id).subscribe(data => {
        console.log(data);
        //
        this.reloadDataSearch();
        this.reloadData();
      });
    }
    else
    {

    }
  }


  editRow(row)
  {
    console.log(row);
    this.router.navigate(['/gestion-comptes/update/'+row.id]);
  }

}

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["gestion-comptes-gestion-comptes-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/gestion-comptes/add/add.component.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/gestion-comptes/add/add.component.html ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<section class=\"content\">\n    <div class=\"container-fluid\">\n        <div class=\"block-header\">\n            <div class=\"row\">\n                <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">\n                    <ul class=\"breadcrumb breadcrumb-style \">\n                        <li class=\"breadcrumb-item\">\n                            <h4 class=\"page-title\">Ajouter Un Compte</h4>\n                        </li>\n                        <li class=\"breadcrumb-item bcrumb-1\">\n                            <a routerLink=\"/dashboard/main\">\n                                <i class=\"fas fa-home\"></i> Home</a>\n                        </li>\n                        <li class=\"breadcrumb-item bcrumb-2\">\n                            <a href=\"#\" onClick=\"return false;\">ajouter-compte</a>\n                        </li>\n                    \n                    </ul>\n                </div>\n            </div>\n        </div>\n        <div class=\"row clearfix\">\n            <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\n                <div class=\"card\">\n                   \n                    <div class=\"body\">\n                        <form [formGroup]=\"addCompteForm\" (ngSubmit)=\"addCompte(addCompteForm)\">\n                            <div class=\"form-group\">\n                                <label>Username </label>\n                                <input type=\"text\" formControlName=\"username\" class=\"form-control\"/>\n                            </div>\n                            <div class=\"form-group\">\n                                <label>Email </label>\n                                <input type=\"email\" formControlName=\"email\" class=\"form-control\"/>\n                            </div>\n                            <div *ngIf=\"!isUpdate\" class=\"form-group\">\n                                <label>Password </label>\n                                <input type=\"password\" formControlName=\"password\" class=\"form-control\"/>\n                            </div>\n                            <div class=\"form-group\">\n                                <label>Status</label>\n                                <select class=\" select2\" formControlName=\"role\" data-placeholder=\"Select\" [(ngModel)]=\"t\">\n                                    <option disabled></option>\n                                    <option value=\"ADMIN\">ADMIN</option>\n                                    <option>UTILISATEUR</option>\n\n                                </select>\n                            </div>\n                            \n                            \n                            <div class=\"form-group\">\n                                <button class=\"btn btn-primary\" >Register</button>\n                            </div>\n                        </form>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n    </section>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/gestion-comptes/list/list.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/gestion-comptes/list/list.component.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<section class=\"content\">\n    <div class=\"container-fluid\">\n        <!-- Basic Examples -->\n       <!-- <div class=\"alert alert-info\">\n            Advance table component is develop using <strong>ngx-datatable</strong> angular plugin. Main features of\n            this table is search record in table, add new record, edit record, delete record, sorting data by\n            ascending and descending, pagination, and many more. You have just replace table static json data with your\n            dynamic json data comes from your api.\n        </div>-->\n        <div class=\"row clearfix\">\n            <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\n                <div class=\"card\">\n                    <div class=\"body\">\n                        <div class=\"row\">\n                            <div class=\"col-sm-12\">\n                                <div class=\"ngxTableHeader\">\n\n                                    <ul class=\"header-buttons-left m-l-5\">\n                                        <li class=\"dropdown m-l-20\">\n                                            <h2>\n                                                <strong>Liste Des Comptes</strong></h2>\n                                        </li>\n                                        <li class=\"dropdown m-l-20\">\n                                            <label for=\"search-input\"><i\n                                                    class=\"material-icons search-icon\">search</i></label>\n                                            <input placeholder=\"Search\" type=\"text\" class=\"browser-default search-field\"\n                                                (keyup)='filterDatatable($event)' aria-label=\"Search box\">\n                                        </li>\n                                    </ul>\n\n                                    <ul class=\"header-buttons m-r-20\">\n                                        <li>\n                                            <div class=\"icon-button-demo\">\n                                                \n                                                <button class=\"btn bg-blue btn-circle waves-effect waves-circle waves-float\">\n                                                    <a routerLink=\"/gestion-comptes/new\"><i class=\"material-icons col-white\" style=\"padding-top: 1px\"> add</i></a>\n                                                </button>\n                                            </div>\n                                        </li>\n\n                                    </ul>\n                                </div>\n                                <ngx-datatable #table class=\"material\" [rows]=\"accounts\" [columns]=\"columns\"\n                                    [sortType]=\"'multi'\" [columnMode]=\"'force'\" [headerHeight]=\"50\" [footerHeight]=\"50\"\n                                    [rowHeight]=\"'60'\" [limit]=\"10\">\n                                    <!-- user image -->\n                                    \n                                    <ngx-datatable-column name=\"username\" [width]=\"130\"></ngx-datatable-column>\n                                    <ngx-datatable-column name=\"email\" [width]=\"130\"></ngx-datatable-column>\n                                    <ngx-datatable-column name=\"password\" [width]=\"130\"></ngx-datatable-column>\n                                    <ngx-datatable-column name=\"roles\" [width]=\"130\"></ngx-datatable-column>\n\n                                    <!-- <ngx-datatable-column *ngFor=\"let col of columns\" [name]=\"col.name\">\n                                    </ngx-datatable-column> -->\n                                    <!-- action buttons -->\n\n\n                                    <ngx-datatable-column name=\"Actions\" sortable=\"false\" [width]=\"120\">\n                                        <ng-template let-value=\"value\" let-row=\"row\" ngx-datatable-cell-template>\n                                            <span>\n                                                <button class=\"btn tblActnBtn h-auto\" data-toggle=\"modal\"\n                                                    data-target=\"#editModal\" (click)='editRow(row)'>\n                                                    <i class=\"material-icons\">mode_edit</i>\n                                                </button>\n                                                <button class=\"btn tblActnBtn h-auto\" (click)='deleteRow(row)'>\n                                                    <i class=\"material-icons\">delete</i>\n                                                </button>\n                                                <!--\n                                                    <button class=\"btn tblActnBtn h-auto\" (click)='startFormation(row)'>\n                                                    <i class=\"fas fa-play\"></i>\n                                                </button>\n                                            -->\n                                            </span>\n                                        </ng-template>\n                                    </ngx-datatable-column>\n\n                                   \n                                </ngx-datatable>\n\n                                <!-- show cv Modal Window -->\n                             \n                                <!-- Edit Record Modal Window -->\n\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</section>\n");

/***/ }),

/***/ "./src/app/gestion-comptes/add/add.component.sass":
/*!********************************************************!*\
  !*** ./src/app/gestion-comptes/add/add.component.sass ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2dlc3Rpb24tY29tcHRlcy9hZGQvYWRkLmNvbXBvbmVudC5zYXNzIn0= */");

/***/ }),

/***/ "./src/app/gestion-comptes/add/add.component.ts":
/*!******************************************************!*\
  !*** ./src/app/gestion-comptes/add/add.component.ts ***!
  \******************************************************/
/*! exports provided: AddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddComponent", function() { return AddComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_authentication_services_authentication_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/authentication/services/authentication.service */ "./src/app/authentication/services/authentication.service.ts");





let AddComponent = class AddComponent {
    constructor(router, bf, auth, activatedRoute) {
        this.router = router;
        this.bf = bf;
        this.auth = auth;
        this.activatedRoute = activatedRoute;
        this.isUpdate = false;
        this.t = "ADMIN";
        this.addCompteForm = this.bf.group({
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
            role: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]()
        });
    }
    ngOnInit() {
        $('select').formSelect();
        let pathId = this.activatedRoute.snapshot.paramMap.get("id");
        if (pathId) {
            this.isUpdate = true;
            this.auth.get(parseInt(pathId)).subscribe(data => {
                let initialRole = "UTILISATEUR";
                console.log(data.roles);
                //
                data.roles.forEach(r => {
                    if (r.name === "ROLE_ADMIN") {
                        initialRole = "ADMIN";
                    }
                });
                //
                this.addCompteForm = this.bf.group({
                    id: [data.id],
                    username: [data.username],
                    email: [data.email],
                    password: [data.password],
                    role: [initialRole]
                });
                //
                /*
                this.addCompteForm.controls['role'].setValue("ADMIN", {onlySelf: true});
                this.addCompteForm.controls['role'].updateValueAndValidity();
                */
            });
        }
    }
    addCompte(form) {
        console.log(form);
        if (!this.isUpdate) {
            let u = {
                "username": form.value.username,
                "password": form.value.password,
                "email": form.value.email,
                "role": ["user"]
            };
            if (form.value.role === 'ADMIN') {
                u.role.push("admin");
            }
            console.log(u);
            this.auth.signUp(u).subscribe(data => {
                console.log(data);
                form.reset();
                this.showNotification("bg-green", "Add Record Successfully", "bottom", "right", "animated fadeInRight", "animated fadeOutRight");
                this.router.navigate(['gestion-comptes/accounts']);
            });
        }
        else {
            let u = {
                "id": form.value.id,
                "username": form.value.username,
                //"password": form.value.password,
                "email": form.value.email,
                "role": ["user"]
            };
            if (form.value.role === 'ADMIN') {
                u.role.push("admin");
            }
            console.log(u);
            this.auth.update(u).subscribe(data => {
                console.log(data);
                form.reset();
                this.showNotification("bg-orange", "Update Record Successfully", "bottom", "right", "animated fadeInRight", "animated fadeOutRight");
                this.router.navigate(['gestion-comptes/accounts']);
            });
        }
    }
    clear() {
        this.addCompteForm.reset();
    }
    showNotification(colorName, text, placementFrom, placementAlign, animateEnter, animateExit) {
        if (colorName === null || colorName === '') {
            colorName = 'bg-black';
        }
        if (text === null || text === '') {
            text = 'Turning standard Bootstrap alerts';
        }
        if (animateEnter === null || animateEnter === '') {
            animateEnter = 'animated fadeInDown';
        }
        if (animateExit === null || animateExit === '') {
            animateExit = 'animated fadeOutUp';
        }
        var allowDismiss = true;
        $.notify({
            message: text
        }, {
            type: colorName,
            allow_dismiss: allowDismiss,
            newest_on_top: true,
            timer: 1000,
            placement: {
                from: placementFrom,
                align: placementAlign
            },
            animate: {
                enter: animateEnter,
                exit: animateExit
            },
            template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (allowDismiss ? "p-r-35" : "") + '" role="alert">' +
                '<span data-notify="icon"></span> ' +
                '<span data-notify="title">{1}</span> ' +
                '<span data-notify="message">{2}</span>' +
                '<div class="progress" data-notify="progressbar">' +
                '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                '</div>' +
                '<a href="{3}" target="{4}" data-notify="url"></a>' +
                '</div>'
        });
    }
};
AddComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: src_app_authentication_services_authentication_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] }
];
AddComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-add',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./add.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/gestion-comptes/add/add.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./add.component.sass */ "./src/app/gestion-comptes/add/add.component.sass")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], src_app_authentication_services_authentication_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
], AddComponent);



/***/ }),

/***/ "./src/app/gestion-comptes/gestion-comptes-routing.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/gestion-comptes/gestion-comptes-routing.module.ts ***!
  \*******************************************************************/
/*! exports provided: GestionComptesRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GestionComptesRoutingModule", function() { return GestionComptesRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _add_add_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add/add.component */ "./src/app/gestion-comptes/add/add.component.ts");
/* harmony import */ var _list_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./list/list.component */ "./src/app/gestion-comptes/list/list.component.ts");





const routes = [
    {
        path: '',
        redirectTo: 'accounts',
        pathMatch: 'full'
    },
    {
        path: 'accounts',
        //component:LoginComponent ,
        component: _list_list_component__WEBPACK_IMPORTED_MODULE_4__["ListComponent"]
    },
    {
        path: 'new',
        //component:LoginComponent ,
        component: _add_add_component__WEBPACK_IMPORTED_MODULE_3__["AddComponent"]
    },
    {
        path: 'update/:id',
        //component:LoginComponent ,
        component: _add_add_component__WEBPACK_IMPORTED_MODULE_3__["AddComponent"]
    },
];
let GestionComptesRoutingModule = class GestionComptesRoutingModule {
};
GestionComptesRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], GestionComptesRoutingModule);



/***/ }),

/***/ "./src/app/gestion-comptes/gestion-comptes.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/gestion-comptes/gestion-comptes.module.ts ***!
  \***********************************************************/
/*! exports provided: GestionComptesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GestionComptesModule", function() { return GestionComptesModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _gestion_comptes_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./gestion-comptes-routing.module */ "./src/app/gestion-comptes/gestion-comptes-routing.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _add_add_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./add/add.component */ "./src/app/gestion-comptes/add/add.component.ts");
/* harmony import */ var _list_list_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./list/list.component */ "./src/app/gestion-comptes/list/list.component.ts");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @swimlane/ngx-datatable */ "./node_modules/@swimlane/ngx-datatable/fesm2015/swimlane-ngx-datatable.js");








let GestionComptesModule = class GestionComptesModule {
};
GestionComptesModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_add_add_component__WEBPACK_IMPORTED_MODULE_5__["AddComponent"], _list_list_component__WEBPACK_IMPORTED_MODULE_6__["ListComponent"]],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
            _gestion_comptes_routing_module__WEBPACK_IMPORTED_MODULE_3__["GestionComptesRoutingModule"],
            _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_7__["NgxDatatableModule"]
        ],
        providers: []
    })
], GestionComptesModule);



/***/ }),

/***/ "./src/app/gestion-comptes/list/list.component.sass":
/*!**********************************************************!*\
  !*** ./src/app/gestion-comptes/list/list.component.sass ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2dlc3Rpb24tY29tcHRlcy9saXN0L2xpc3QuY29tcG9uZW50LnNhc3MifQ== */");

/***/ }),

/***/ "./src/app/gestion-comptes/list/list.component.ts":
/*!********************************************************!*\
  !*** ./src/app/gestion-comptes/list/list.component.ts ***!
  \********************************************************/
/*! exports provided: ListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListComponent", function() { return ListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @swimlane/ngx-datatable */ "./node_modules/@swimlane/ngx-datatable/fesm2015/swimlane-ngx-datatable.js");
/* harmony import */ var src_app_authentication_services_authentication_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/authentication/services/authentication.service */ "./src/app/authentication/services/authentication.service.ts");





let ListComponent = class ListComponent {
    constructor(auth, router) {
        this.auth = auth;
        this.router = router;
        this.rows = [];
        this.selectedName = "";
        this.columns = [
            { name: 'username' }, { name: 'email' }, { name: 'password' }, { name: 'roles' }
        ];
        this.allColumns = [{ name: 'username' }, { name: 'email' }, { name: 'password' }, { name: 'roles' }];
        this.accounts = [];
        this.data = [];
        this.filteredData = [];
    }
    //********************************************************************************** */
    ngOnInit() {
        this.reloadDataSearch();
        this.reloadData();
        console.log(this.accounts);
    }
    //************************************************************************************** */
    //*********************************************************************************** */
    //***************************************************************************** */
    reloadData() {
        this.auth.users().subscribe(data => {
            this.accounts = data;
            this.accounts.forEach(u => {
                u.password = "*****";
                let r = "";
                u.roles.forEach(rl => {
                    r += rl.name + " | ";
                });
                u.roles = r;
            });
            console.log(this.accounts);
        });
    }
    //********************************************************* *
    reloadDataSearch() {
        this.auth.users().subscribe(data => this.filteredData = data);
    }
    //*********************************************************************************** */
    //************************************************************************************************* */
    filterDatatable(event) {
        // get the value of the key pressed and make it lowercase
        let val = event.target.value.toLowerCase();
        // get the amount of columns in the table
        let colsAmt = this.columns.length;
        // get the key names of each column in the dataset
        let keys = Object.keys(this.filteredData[0]);
        // assign filtered matches to the active datatable
        this.accounts = this.filteredData.filter(function (item) {
            // iterate through each row's column data
            for (let i = 0; i < colsAmt; i++) {
                // check for a match
                if (item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 || !val) {
                    // found match, return true to add to result set
                    return true;
                }
            }
        });
        // whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }
    getId(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }
    showNotification(colorName, text, placementFrom, placementAlign, animateEnter, animateExit) {
        if (colorName === null || colorName === '') {
            colorName = 'bg-black';
        }
        if (text === null || text === '') {
            text = 'Turning standard Bootstrap alerts';
        }
        if (animateEnter === null || animateEnter === '') {
            animateEnter = 'animated fadeInDown';
        }
        if (animateExit === null || animateExit === '') {
            animateExit = 'animated fadeOutUp';
        }
        var allowDismiss = true;
        $.notify({
            message: text
        }, {
            type: colorName,
            allow_dismiss: allowDismiss,
            newest_on_top: true,
            timer: 1000,
            placement: {
                from: placementFrom,
                align: placementAlign
            },
            animate: {
                enter: animateEnter,
                exit: animateExit
            },
            template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (allowDismiss ? "p-r-35" : "") + '" role="alert">' +
                '<span data-notify="icon"></span> ' +
                '<span data-notify="title">{1}</span> ' +
                '<span data-notify="message">{2}</span>' +
                '<div class="progress" data-notify="progressbar">' +
                '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                '</div>' +
                '<a href="{3}" target="{4}" data-notify="url"></a>' +
                '</div>'
        });
    }
    ///*********************************************************************** */
    deleteRow(row) {
        console.log(row);
        if (window.confirm("êtes-vous sûr de vouloir supprimer ?")) {
            this.auth.delete(row.id).subscribe(data => {
                console.log(data);
                //
                this.reloadDataSearch();
                this.reloadData();
            });
        }
        else {
        }
    }
    editRow(row) {
        console.log(row);
        this.router.navigate(['/gestion-comptes/update/' + row.id]);
    }
};
ListComponent.ctorParameters = () => [
    { type: src_app_authentication_services_authentication_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_3__["DatatableComponent"], { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_3__["DatatableComponent"])
], ListComponent.prototype, "table", void 0);
ListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-list',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./list.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/gestion-comptes/list/list.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./list.component.sass */ "./src/app/gestion-comptes/list/list.component.sass")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_authentication_services_authentication_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
], ListComponent);



/***/ })

}]);
//# sourceMappingURL=gestion-comptes-gestion-comptes-module-es2015.js.map